<%@ page language="java" contentType="text/html; charset=gb2312"
    pageEncoding="gb2312"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title>Attractions Administration</title>
</head>
<body>
	<%@ include file="admin_logo.jsp" %><br>
	<h4 align="center">Attractions Administration</h4>
	<table border="1" cellpadding="0" cellspacing="0" width="900" align="center">
		<c:forEach items="${sight}" var="v">
			<tr>
				<td width="80%" align="left">&nbsp;&nbsp;&nbsp;&nbsp;${v.name }</td>
				<td width="20%" align="center">
					<a href="adminsightmodify.do?id=${v.id}">Update</a>&nbsp;&nbsp;
					<a href="adminsightdelete.do?id=${v.id }">Delete</a>
				</td>
			</tr>
		</c:forEach>
		<tr>
			<td colspan="2" align="center">
				<a href="admin_addsight.jsp">Add</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
	</table>
</body>
</html>