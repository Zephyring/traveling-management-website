<%@ page language="java" contentType="text/html; charset=gb2312"
    pageEncoding="gb2312"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title>Book Tickets</title>
</head>
<body>
	<%@ include file="logo.jsp" %>
	<br>
	<form action="book.do" method="post" name="book">
		<table align="center" border="1" width="900">
			<tr>
				<td align="right">Sight: </td>
				<td align="left">
					<input type="text" name="sightName" value="${ticket.sight}" readonly/>
				</td>
			</tr>
			<tr>
				<td align="right">City: </td>
				<td align="left">
					<input type="text" name="city" value="${ticket.city}" readonly/>
					<input type="hidden" name="ticketId" value="${ticket.id}" readonly/>
				</td>
			</tr>
			<tr>
				<td align="right">Unit Price: </td>
				<td align="left">
					<input type="text" name="price" value="${ticket.price}" readonly/>
				</td>
			</tr>
			<tr>
				<td align="right">Ticket Count: </td>
				<td align="left">
					<input type="text" name="count" value="1"/>
				</td>
			</tr>
			<tr>
				<td align="right">Ticket Expiration Date: </td>
				<td align="left">
					<input type="text" name="ticketExpirationDate" value="${today}"> 
				</td>
			</tr>
			<tr>
				<td align="right">Use Reward Point to Book: </td>
				<td align="left">
					<input type="checkbox" name="useReward"> 
				</td>
			</tr>
			<tr align="right">
				<td>Remain Reward Point: </td><td align="left"><input type="text" name="rewardPoint" value="${user.rewardPoint } " readonly /></td>
			</tr>
			<tr align="right">
				<td>Membership: </td><td align="left"><input type="text" name="membership" value="${user.membership }" readonly /></td>
			</tr>
			<tr align="right">
				<td>Credit Card Number: </td><td align="left"><input type="text" name="creditCardNumber" value="${user.creditCardNumber }" /></td>
			</tr>
			<tr>
				<td align="right">Security Code: </td><td><input type="text" name="securityCode" value=""/></td>
			</tr>
			<tr>
				<td align="right">Card Expiration Date: </td><td><input type="text" name="cardExpirationDate" value=""/></td>
			</tr>
			<tr>
				<td align="right">&nbsp;<input type="submit" value="Submit"/>&nbsp;&nbsp;</td>
				<td align="left">&nbsp;&nbsp;<input type="reset" value="Reset"/></td>
			</tr>
		</table>
	</form>
</body>
</html>