<%@ page language="java" contentType="text/html; charset=gb2312"
    pageEncoding="gb2312"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="logo.jsp" %>
	<c:if test="${empty sightList}">
		<table border="0" align="center"><tr><td><font color="#991188">No attraction info., please check it later.</font></td></tr></table>
	</c:if>
	<c:forEach items="${sightList}" var="v">
		<p/>
		<table align="center" width="700">
			<tr>
				<td><img src="img/default_pic_small.gif"/></td>
				<td align="left" width="700">
					<table border="1" width="700" height="120">
						<tr>
							<td width="25%" align="center">Name: </td>
							<td width="75%" align="left"><a href="indexsight.do?id=${v.id }">${v.name}</a></td>
						</tr>
						<tr>
							<td align="center">Ticket Price: </td><td>${v.price }</td>
						</tr>
						<tr>
							<td align="center">Company: </td><td>${v.company}</td>
						</tr>
						<tr>
							<td align="center">Description: </td><td><a href="indexsight.do?id=${v.id }">${v.info}</a></td>
						</tr>
		</table></td></tr></table>
	</c:forEach>
</body>
</html>