<%@ page language="java" import="java.util.*" pageEncoding="gb2312"%>
<%@ taglib prefix="logic" uri="http://struts.apache.org/tags-logic" %>
<%@ taglib prefix="bean" uri="http://struts.apache.org/tags-bean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>SightseeingSys</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  	<%@ include file="logo.jsp" %><br>
    <table width="900" height="800" border="0" cellpadding="0" cellspacing="0" align="center">
		<tr>
			<td width="190">
				<table width="190" height="800" border="0" cellpadding="0" cellspacing="0">
					<tr height="132">
						<td>
							<table height="130"  width="190" border="1" cellpadding="0" cellspacing="0" bordercolor="#167bd6">
								<tr height="28">
									<td width="190" background="img/left_bg_login.jpg">
									</td>
								</tr>
								<!-- #f7fbff -->
								<c:if test="${empty user}">
									<tr height="102">
										<td bgcolor="#f7fbff" align="center">
											<form action="login.do" method="post" name="form1">
												Username: <input type="text" name="name" size="15"><br>
												Password: <input type="password" name="password" id="name" size="15"><br>
												&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value="Login" onclick="form1.submit()"/>
												&nbsp;&nbsp;&nbsp;<a href="register_user.jsp"><input type="button" value="Register"/></a><br>
												${message }
											</form>
										</td>
									</tr>
								</c:if>
								<c:if test="${!empty user}">
									<tr height="102" align="center">
										<td>
										${user.name }<br>
										<a href="confirm.do">My Account</a>&nbsp;|&nbsp;<a href="logout.do">logout</a>
										</td>
									</tr>
								</c:if>
							</table>
						</td>
					</tr>
					<tr height="3"><td width="190"></td></tr>
					<tr height="665">
						<td width="190">
							<table height="665"  width="190" border="1" cellpadding="0" cellspacing="0" bordercolor="#167bd6">
								<tr height="28">
									<td width="190" background="img/left_bg_links.jpg">
									</td>
								</tr>
								<tr height="637">
									<td width="190" valign="top">
										<table border="0" height="500">
											<tr>
												<td><a href="testData.jsp">Test Data</a></td>
											</tr>
											<tr>
												<td><a href="http://www.google.com">Google</a></td>
											</tr>
											<tr>
												<td><a href="http://www.expedia.com">Expedia</a></td>
											</tr>
											<tr>
												<td><a href="http://www.cheapPrice.com">cheapPrice</a></td>
											</tr>
											<tr>
												<td><a href="http://www.priceline.com">Priceline</a></td>
											</tr>
											<tr>
												<td><a href="http://www.tripadvisor.com">TripAdvisor</a></td>
											</tr>
											<tr>
												<td><a href="http://www.google.com">Google</a></td>
											</tr>
											<tr>
												<td><a href="http://www.expedia.com">Expedia</a></td>
											</tr>
											<tr>
												<td><a href="http://www.cheapPrice.com">cheapPrice</a></td>
											</tr>
											<tr>
												<td><a href="http://www.priceline.com">Priceline</a></td>
											</tr>
											<tr>
												<td><a href="http://www.tripadvisor.com">TripAdvisor</a></td>
											</tr>
										</table>
										<table>
											<tr>
												<td>
													<img src="img/sight.jpg"/>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td width="3" height="800">&nbsp;</td>
			<!-- right -->
			<td width="707" height="800">
				<table border="0" cellpadding="0" cellspacing="0" width="707" height="330" >
					<tr><td>
						<table border="0" cellpadding="0" cellspacing="0"  width="705" height="330">
							<tr height="30">
								<td background="img/attractions.jpg"></td>
							</tr>
							<tr>
								<td width="550" valign="top">
									<div class="second">
										<table border="0" cellpadding="0" cellspacing="0">
											<tr><td><table>
												<c:forEach items="${sight}" var="v" end="30" varStatus="vs">
													<c:if test="${vs.count%5==1 && vs.count>1}">
														<tr height="10">
													</c:if>
													<td align="center" width="100" bgColor="${var1 }"><a href="indexsight.do?id=${v.id}">${v.name }</a></td>
													<td> �� </td>
													<c:if test="${vs.count % 5 == 0}">
													</c:if>
												</c:forEach>
											</table></td></tr>
										</table>
									</div>
								</td>
							</tr>
						</table>
					</td></tr>
					
				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="707" height="240">
					<tr height="30">
						<td background="img/cities.jpg"></td>
					</tr>
					<tr height="210">
						<td width="550" valign="top">
							<table width="707" border="0" cellpadding="0" cellspacing="0">
							<tr><td><table>
												<c:forEach items="${sightName}" var="v" end="19" varStatus="vs">
													<c:if test="${vs.count%5==1 && vs.count>1}">
														<tr height="10">
													</c:if>
													<td align="center" width="100" bgColor="${var1 }"><a href="indexcity.do?id=${v.id}">${v.city }</a></td>
													<td> �� </td>
													<c:if test="${vs.count % 5 == 0}">
													</c:if>
												</c:forEach>
								</table></td>
							</tr>
							</table>
						</td>
					</tr>
				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="707" height="200" bordercolor="#1b930d">
					<tr height="30">
						<td background="img/posts.jpg"></td>
					</tr>
					<tr height="200">
						<td>
							<table border="0" cellpadding="0" cellspacing="0" width="707" height="190">
								<tr height="20">
									<td align="left" valign="top" width="607">Title</td><td align="left" valign="top" width="100">Date</td>
								</tr>
								<c:forEach items="${bbsList}" begin="0" end="9" var="v">
									<tr height="20">
										<td align="left" width="607"><a href="seeposts.do?id=${v.id }">${v.name }</a></td>
										<td align="left" width="100">${v.date }</td>
									</tr>
								</c:forEach>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
  </body>
</html>
