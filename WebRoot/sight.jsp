<%@ page language="java" contentType="text/html; charset=gb2312"
    pageEncoding="gb2312"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title>Sights</title>
</head>
<body>
	<%@ include file="logo.jsp" %><br>
	<table border="0" width="900" align="center" >
		<tr><td>
			<form action="querysight.do" method="post">
				City: <input type="text" name="city" size="15"/>&nbsp;&nbsp;
				Attraction: <input type="text" name="name" size="15"/>&nbsp;&nbsp;
				<input type="submit" value="Check"/>
			</form>
		</td></tr>
	</table>
	<c:forEach items="${sight}" var="v">
		<p/>
		<table align="center" width="900">
			<tr>
				<td><img src="img/default_pic_small.gif"/></td>
				<td align="left" width="700">
					<table border="1" width="700" height="120">
						<tr>
							<td width="25%" >Name: </td>
							<td width="75%" align="left"><a href="indexsight.do?id=${v.id }">${v.name}</a></td>
						</tr>
						<tr>
							<td>Ticket Price: </td><td>${v.price }</td>
						</tr>
						<tr>
							<td>Company: </td><td>${v.company }</td>
						</tr>
						<tr>
							<td>Description: </td>
							<td>
								<a href="indexsight.do?id=${v.id }"><font color="black">${v.info}</font></a>
							</td>
						</tr>
		</table></td></tr></table>
	</c:forEach>
</body>
</html>