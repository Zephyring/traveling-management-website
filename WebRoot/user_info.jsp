<%@ page language="java" contentType="text/html; charset=gb2312"
    pageEncoding="gb2312"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title>Personal Info.</title>
</head>
<body>
	<%@ include file="logo.jsp" %>
	<table border="1" align="center" width="900">
		<tr><td>
			<br/>
			Username: ${user.name }<br>
			Name: ${user.tname }<br>
			Telephone: ${user.telephone }<br>
			E-mail: ${user.email }<br>
			Credit Card Number: ${user.creditCardNumber}<br>
			Reward Point: ${user.rewardPoint }<br>
			Membership: ${user.membership }<br>
			<a href="modify_info.jsp">Update || </a>
			<a href="login.do">Index</a>
			<p/>
			<li><font color="#3e44a1">Reservations</font></li><br/>
			<table border="0" cellpadding="0" bordercolor="blue">
				<c:choose>
					<c:when test="${empty userTicket}">
						<tr><td><font color="#339922">No Reservations.</font></td></tr>
					</c:when>
					<c:otherwise>
						<c:forEach items="${userTicket}" var="v" varStatus="vs">
							<c:if test="${vs.count%2==1}"><c:set value="#ffee99" var="c"/></c:if>
							<c:if test="${vs.count%2==0}"><c:set value="#ffffff" var="c"/></c:if>
							<tr>
								<td bgcolor="${c }" width="200">Sight: ${v.sight }</td>
								<td bgcolor="${c }" align="center" width="200">City: ${v.city}</td>
								<td bgcolor="${c }" width="200">Count: ${v.count }</td>
								<td bgcolor="${c }" width="200">Expiration Date: ${v.expirationDate }</td>
								
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</table>
			<p/>
			<table height="50" border="0">
				<tr><td></td></tr>
			</table>
		
		</td></tr>
	</table>
</body>
</html>