package travel.struts.actionforms;

import org.apache.struts.action.ActionForm;

public class BookActionForm extends ActionForm {
	
	private int count;
	
	private int ticketId;
	
	private String ticketExpirationDate;
	
	private String creditCardNumber;
	
	private String securityCode;
	
	private String cardExpirationDate;
	
	private boolean useReward;
	
	public int getTicketId()
	{
		return ticketId;
	}
	
	public void setTicketId(int id)
	{
		this.ticketId=id;
	}
	
	public int getCount()
	{
		return count;
	}
	
	public void setCount(int count)
	{
		this.count=count;
	}
	
	public String getTicketExpirationDate()
	{
		return ticketExpirationDate;
	}
	
	public void setTicketExpirationDate(String ticketExpirationDate)
	{
		this.ticketExpirationDate=ticketExpirationDate;
	}
	
	public String getcreditCardNumber()
	{
		return creditCardNumber;
	}
	
	public void setCreditCardNumber(String creditCardNumber)
	{
		this.creditCardNumber=creditCardNumber;
	}
	
	public String getSecurityCode()
	{
		return securityCode;
	}
	
	public void setSecurityCode(String securityCode)
	{
		this.securityCode=securityCode;
	}
	
	public String getCardExpirationDate()
	{
		return cardExpirationDate;
	}
	
	public void setCardExpirationDate(String cardExpirationDate)
	{
		this.cardExpirationDate=cardExpirationDate;
	}
	
	public boolean getUseReward()
	{
		return useReward;
	}
	
	public void setUseReward(boolean useReward)
	{
		this.useReward=useReward;
	}

	
	
	
}
