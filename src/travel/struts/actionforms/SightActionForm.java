package travel.struts.actionforms;

import org.apache.struts.action.ActionForm;

public class SightActionForm extends ActionForm {
	
	private int id;
	
	private String name;
	
	private String city;
	
	private String company;
	
	private int price;
	
	private String info;
	
	public int getId()
	{
		return id;
	}
	
	public void setId(int id)
	{
		this.id=id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCompany()
	{
		return this.company;
	}
	
	public void setCompany(String c)
	{
		this.company=c;
	}
	
	public int getPrice()
	{
		return price;
	}
	
	public void setPrice(int p)
	{
		this.price=p;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

}
