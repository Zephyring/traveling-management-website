package travel.struts.actionforms;

import org.apache.struts.action.ActionForm;

public class TestDataActionForm extends ActionForm{
	
	private String database;
	
	public String getDatabase()
	{
		return this.database;
	}
	
	public void setDatabase(String database)
	{
		this.database=database;
	}
}
