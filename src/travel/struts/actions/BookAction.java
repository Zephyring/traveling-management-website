package travel.struts.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import travel.struts.data.BookInfo;
import travel.struts.data.User;
import travel.struts.datemanager.TicketManager;
import travel.struts.datemanager.UserManager;

public class BookAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String map = null;
		BookInfo bookInfo=new BookInfo();
		User user=(User)request.getSession().getAttribute("user");
		String previous=user.getMembership();
		if(user==null)
			map="error";
		else
		{
			BeanUtils.copyProperties(bookInfo, form);
			bookInfo.setUserId(user.getId());
			TicketManager.getInstance().bookTicket(bookInfo);
			//after booking, update user_info
			user=UserManager.getInstance().findUserById(user.getId());
			request.getSession().setAttribute("user", user);
			String now=user.getMembership();
			if(previous.equals(now))
				map = "success";
			else map = "membership";
		}
		return mapping.findForward(map);
	}

}
