package travel.struts.actions;

import java.text.DateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import travel.struts.data.Sight;
import travel.struts.data.Ticket;
import travel.struts.data.User;
import travel.struts.datemanager.SightManager;
import travel.struts.datemanager.TicketManager;

public class BookLinkAction extends Action {


	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		//get ticket information by sight id
		int id = Integer.parseInt(request.getParameter("id"));
		Sight sight = SightManager.getInstance().findSightById(id);
		Ticket ticket = TicketManager.getInstance().findTicketBySightName(sight.getName());
		//get current date as default value
		Date date=new Date();
		DateFormat df=DateFormat.getDateInstance();
		String time=df.format(date);
    	request.setAttribute("ticket", ticket);	
		request.setAttribute("today", time);
		return mapping.findForward("success");
	}

}
