package travel.struts.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import travel.struts.data.User;
import travel.struts.data.UserTicket;
import travel.struts.datemanager.SightManager;
import travel.struts.datemanager.UserManager;

public class ConfirmInfoAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String map = "";
		User user = (User)request.getSession().getAttribute("user");
		User user_login = UserManager.getInstance().loginManager(user);
		request.getSession().setAttribute("user", user_login);//update the session	
		if(user != null) 
		{		
			List list=UserManager.getInstance().getUserTicketReservation(user);
			request.setAttribute("userTicket", list);
			map = "success";
		} else {
			map = "login";
		}
		return mapping.findForward(map);

	}
}
