package travel.struts.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import travel.struts.data.User;
import travel.struts.datemanager.UserManager;

public class ModifyUserInfoAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String map = "";
		User user = (User)request.getSession().getAttribute("user");
		if(user == null) {
			map = "login";
		} else {
			BeanUtils.copyProperties(user, form);
			UserManager.getInstance().modifyUserInfo(user);//update the database
			User user_login = UserManager.getInstance().loginManager(user);
			request.getSession().setAttribute("user", user_login);//update the session	
			List list=UserManager.getInstance().getUserTicketReservation(user);
			request.setAttribute("userTicket", list);
			map = "success";
		}
		
		return mapping.findForward(map);
	}
}
