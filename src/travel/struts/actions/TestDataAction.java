package travel.struts.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import travel.struts.data.TestData;
import travel.struts.datemanager.TestDataManager;

public class TestDataAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String map = null;
		TestData test = new TestData();
		BeanUtils.copyProperties(test, form);
		String database=test.getDatabase();
		String data=TestDataManager.getInstance().getDatabyDatabaseName(database);
		if(data==null) {	
			data="No Data!";
		} 
		request.setAttribute("result", data);	
		map = "success";
		return mapping.findForward(map);
	}

}
