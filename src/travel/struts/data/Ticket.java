package travel.struts.data;

public class Ticket {
	
	private int id;
	private String sight;
	private String city;
	private String price;
	
	public int getId()
	{
		return id;
	}
	
	public void setId(int id)
	{
		this.id=id;
	}
	
	public String getSight()
	{
		return sight;
	}
	
	public void setSight(String sightName)
	{
		this.sight=sightName;
	}
	
	public String getCity()
	{
		return city;
	}
	
	public void setCity(String city)
	{
		this.city=city;
	}
	
	public String getPrice()
	{
		return price;
	}
	
	public void setPrice(String price)
	{
		this.price=price;
	}

}
