package travel.struts.data;

public class User {
	private int id;
	
	private String name;
	
	private String tname;
	
	private String password;
	
	private String telephone;
	
	private String email;
	
	private int admin;
	
	private String creditCardNumber;
	
	private int rewardPoint;
	
	private String membership;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getAdmin() {
		return admin;
	}

	public void setAdmin(int admin) {
		this.admin = admin;
	}

	public String getTname() {
		return tname;
	}

	public void setTname(String tname) {
		this.tname = tname;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getCreditCardNumber()
	{
		return creditCardNumber;
	}
	
	public void setCreditCardNumber(String creditCardNumber)
	{
		this.creditCardNumber=creditCardNumber;
	}
	
	public int getRewardPoint()
	{
		return this.rewardPoint;
	}
	
	public void setRewardPoint(int r)
	{
		this.rewardPoint=r;
	}
	
	public String getMembership()
	{
		return this.membership;
	}
	
	public void setMembership(String m)
	{
		this.membership=m;
	}
	
}
