package travel.struts.data;

public class UserTicket {
	
	private String sight;
	
	private String city;
	
	private int count;
	
	private String expirationDate;
	
	public String getSight()
	{
		return this.sight;
	}
	
	public void setSight(String s)
	{
		this.sight=s;
	}
	
	public String getCity()
	{
		return this.city;
	}
	
	public void setCity(String c)
	{
		this.city=c;
	}

	public int getCount()
	{
		return this.count;
	}
	
	public void setCount(int c)
	{
		this.count=c;
	}
	
	public String getExpirationDate()
	{
		return this.expirationDate;
	}
	
	public void setExpirationDate(String expirationDate)
	{
		this.expirationDate=expirationDate;
	}
}
