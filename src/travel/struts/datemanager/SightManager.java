package travel.struts.datemanager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import travel.struts.data.Sight;
import travel.struts.factory.DBOFactory;

public class SightManager {
	
	private static SightManager instance = new SightManager();
	
	public static SightManager getInstance() {
		return instance;
	}
	
	/**
	 * 列出所有景点名称
	 * @return
	 */
	public List listSightName() {
		List sightList = DBOFactory.getInstance().getDBO().listSightName();
		return sightList;
	}
	
	
	/**
	 * 通过ID查询景点信息
	 * @param id
	 * @return
	 */
	public Sight findSightById(int id) {
		Sight sight = DBOFactory.getInstance().getDBO().findSightById(id);
		return sight;
	}
	
	
	/**
	 * 通过城市名称查询该城市的景点
	 * @param cityName
	 * @return
	 */
	public List listSightByCityName(String cityName) {
		List sightList = DBOFactory.getInstance().getDBO().listSightByCityName(cityName);
		return sightList;
	}
	
	
	public List listSightBySightName(String sightName) {
		List sightList = DBOFactory.getInstance().getDBO().listSightBySightName(sightName);
		return sightList;
	}
	
	/**
	 * 根据城市名查询景点
	 * @param cityName
	 * @param sightName
	 * @return
	 */
	public List listSightBySightCityName(String cityName,String sightName) {
		List sightList = DBOFactory.getInstance().getDBO().listSightBySightCityName(cityName, sightName);
		return sightList;
	}
	
	
	/**
	 * 更新景点信息
	 * @param sight
	 */
	public void updateSightInfo(Sight sight) {
		DBOFactory.getInstance().getDBO().updateSightInfo(sight);
	}
	
	/**
	 * 根据景点ID删除景点信息
	 * @param sightId
	 */
	public void deleteSightById(int sightId) {
		DBOFactory.getInstance().getDBO().deleteSightById(sightId);
	}
	
	/**
	 * 增加景点信息
	 * @param sight
	 */
	public void sightAdd(Sight sight) {
		DBOFactory.getInstance().getDBO().sightAdd(sight);
	}
	
	/**
	 * 只取出景点的名称,为首页所用.
	 * @return
	 */
	public List listCityNameForIndex() {
		return DBOFactory.getInstance().getDBO().listCityNameForIndex();
	}
	
	
}
