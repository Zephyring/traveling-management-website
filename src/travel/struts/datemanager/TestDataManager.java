package travel.struts.datemanager;

import travel.struts.factory.DBOFactory;

public class TestDataManager {
	
private static TestDataManager instance = new TestDataManager();
	
	public static TestDataManager getInstance() {
		return instance;
	}
	
	/**
	 * 获取数据库
	 * @param database name
	 * @return all data
	 */
	public String getDatabyDatabaseName(String database) {
		String data = DBOFactory.getInstance().getDBO().getDatabyDatabaseName(database);
		return data;
	}

}
