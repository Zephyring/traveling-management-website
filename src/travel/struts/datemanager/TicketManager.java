package travel.struts.datemanager;

import travel.struts.data.BookInfo;
import travel.struts.data.Sight;
import travel.struts.data.Ticket;
import travel.struts.factory.DBOFactory;

public class TicketManager {
	
	private static TicketManager instance=null;
	
	public static TicketManager getInstance()
	{
		if(instance==null)
		{
			instance=new TicketManager();
		}
		return instance;
	}
	
	/**
	 * 通过sight name查询ticket
	 */
	public Ticket findTicketBySightName(String sightName)
	{
		Ticket ticket = DBOFactory.getInstance().getDBO().findTicketBySightName(sightName);
		return ticket;
	}
	
	/**
	 * 给用户预定ticket
	 */
	public void bookTicket(BookInfo bookInfo)
	{
		DBOFactory.getInstance().getDBO().bookTicket(bookInfo);
	}

}
