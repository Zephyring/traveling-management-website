package travel.struts.db;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import java.util.Set;
import java.util.HashSet;

import travel.struts.data.BBS;
import travel.struts.data.BookInfo;
import travel.struts.data.Sight;
import travel.struts.data.Ticket;
import travel.struts.data.User;
import travel.struts.data.UserTicket;
import travel.struts.factory.DBOFactory;

public class DateBaseOperator {

	/*
	 *------------------------------------------------------------------------------------------------------------
	 * 论坛模块
	 * 
	 */
	
	public List bbsList() {
		String sql = "select * from bbs";
		List bbsList = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConn();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				BBS bbs = new BBS();
				bbs.setId(rs.getInt("id"));
				bbs.setName(rs.getString("name"));
				bbs.setContent(rs.getString("content"));
				bbs.setDate(rs.getString("date"));
				bbs.setAccess(rs.getInt("access"));
				bbs.setReply(rs.getInt("reply"));
				bbs.setUid(rs.getInt("uid"));
				bbsList.add(bbs);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(stmt);
			DB.closeConn(conn);
		}
		return bbsList;
	}
	
	public BBS findPostById(int postId) {
		String sql = "select * from bbs where id=?";
		BBS post = new BBS();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, postId);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				post.setId(rs.getInt("id"));
				post.setName(rs.getString("name"));
				post.setContent(rs.getString("content"));
				post.setDate(rs.getString("date"));
				post.setAccess(rs.getInt("access"));
				post.setReply(rs.getInt("reply"));
				post.setUid(rs.getInt("uid"));
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
		return post;
	}
	
	public List listReplyByPostId(int postId) {
		String sql = "select * from reply where id=?";
		List replyList = new ArrayList();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, postId);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				BBS bbs = new BBS();
				bbs.setId(rs.getInt("id"));
				bbs.setUid(rs.getInt("uid"));
				User user = this.findUserById(bbs.getUid());
				bbs.setUname(user.getName());
				bbs.setContent(rs.getString("content"));
				bbs.setDate(rs.getString("date"));
				bbs.setRid(rs.getInt("rid"));
				replyList.add(bbs);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
		return replyList;
	}
	
	/**
	 * 回帖
	 * @param postId
	 * @param userId
	 * @param bbs
	 */
	public void setPostReply(int postId, int userId, BBS bbs) {
		String sql = "insert into reply(id, uid, content, date, rid) values(?, ?, ?, ?, ?)";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, postId);
			pstmt.setInt(2, userId);
			pstmt.setString(3, bbs.getContent());
			pstmt.setString(4, bbs.getDate());
			pstmt.setInt(5, bbs.getRid());
			pstmt.executeUpdate();
			this.addRewardPointByUserId(userId, 1);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
	}
	
	public void addAccess(int postId) {
		String sql = "update bbs set access=? where id=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			BBS bbs = this.findPostById(postId);
			bbs.setAccess(bbs.getAccess() + 1);
			pstmt.setInt(1, bbs.getAccess());
			pstmt.setInt(2, postId);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
	}
	
	public void addReply(int postId) {
		String sql = "update bbs set reply=? where id=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			BBS bbs = this.findPostById(postId);
			bbs.setReply(bbs.getReply() + 1);
			pstmt.setInt(1, bbs.getReply());
			pstmt.setInt(2, postId);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
	}
	
	public void appearPost(BBS bbs) {
		Connection conn = null;
		conn = DB.getConn();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql = "select max(id) as maxId from bbs";
			pstmt = conn.prepareStatement(sql);
			rs=pstmt.executeQuery(sql);
			while(rs.next())
			{
				bbs.setId(rs.getInt("maxId")+1);
			}
			
			sql = "insert into bbs(id, name, content, date, access, reply, uid) values(?, ?, ?, ?, ?, ?, ?)";
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, bbs.getId());
			pstmt.setString(2, bbs.getName());
			pstmt.setString(3, bbs.getContent());
			pstmt.setString(4, bbs.getDate());
			pstmt.setInt(5, bbs.getAccess());
			pstmt.setInt(6, bbs.getReply());
			pstmt.setInt(7, bbs.getUid());
			pstmt.executeUpdate();
			this.addRewardPointByUserId(bbs.getUid(), 2);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
	}
	
	/**
	 * 删贴
	 * @param postId
	 */
	public void deletePostById(int postId) {
		String sql = "delete from bbs where id=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, postId);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
	}
	
	public void deleteReplyByid(int replyId) {
		String sql = "delete from reply where rid=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, replyId);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
	}
	
	/*
	 * 用户管理模块
	 * -----------------------------------------------------------------------------------------------------
	 */
	/**
	 * 
	 * 登陆时用户查询
	 * @param user
	 * @return
	 */
	public User findUserByName(User user) {
		String userName = user.getName();
		User user1 = new User();
		String sql = "select * from user_info where name=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, userName);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				user1.setId(rs.getInt("user_id"));
				user1.setName(rs.getString("name"));
				user1.setPassword(rs.getString("password"));
				user1.setTname(rs.getString("tname"));
				user1.setTelephone(rs.getString("telephone"));
				user1.setAdmin(rs.getInt("admin"));
				user1.setEmail(rs.getString("email"));
				user1.setCreditCardNumber(rs.getString("creditCardNumber"));
				user1.setRewardPoint(rs.getInt("rewardPoint"));
				user1.setMembership(rs.getString("membership"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
		
		return user1;
	}
	
	
	/**
	 * 游客添加模块
	 */
	public void userAdd(User user) {	
		Connection conn = null;
		conn = DB.getConn();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			
			//first get the count of user to define user_id
			String sql = "select count(*) as user_id from user_info";
			pstmt = conn.prepareStatement(sql);
			rs=pstmt.executeQuery(sql);
			while(rs.next())
			{
				user.setId(rs.getInt("user_id"));
			}
			//initialise rewardPoint to be 0, and membership to be silver, admin to be 1
			user.setRewardPoint(0);
			user.setMembership("silver");
			user.setAdmin(1);
			
			//insert info into user_info
			sql = "insert into user_info(user_id, name, tname, password, telephone, admin, email, creditCardNumber, rewardPoint, membership) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";	
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, user.getId());
			pstmt.setString(2, user.getName());
			pstmt.setString(3, user.getTname());
			pstmt.setString(4, user.getPassword());
			pstmt.setString(5, user.getTelephone());
			pstmt.setInt(6, user.getAdmin());
			pstmt.setString(7, user.getEmail());
			pstmt.setString(8, user.getCreditCardNumber());
			pstmt.setInt(9, user.getRewardPoint());
			pstmt.setString(10, user.getMembership());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
	}
	
	/**
	 * 管理员、用户密码修改模块（其它修改以后增加）只能修改不能修改用户名及权限
	 * @param user
	 */
	public void UserModify(User user) {
		String sql = "update user_info set password=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, user.getPassword());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
	}
	
	/**
	 * 修改游客个人信息
	 * @param user
	 */
	public void modifyUserInfo(User user) {
		String sql = "update user_info set password=?, tname=?, telephone=?, email=?, creditCardNumber=? where user_id=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, user.getPassword());
			pstmt.setString(2, user.getTname());
			pstmt.setString(3, user.getTelephone());
			pstmt.setString(4, user.getEmail());
			pstmt.setString(5, user.getCreditCardNumber());
			pstmt.setInt(6, user.getId());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
	}
	
	public User findUserById(int userId) {
		User user = new User();
		String sql = "select * from user_info where user_id=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, userId);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				user.setId(rs.getInt("user_id"));
				user.setName(rs.getString("name"));
				user.setTname(rs.getString("tname"));
				user.setPassword(rs.getString("password"));
				user.setTelephone(rs.getString("telephone"));
				user.setAdmin(rs.getInt("admin"));
				user.setEmail(rs.getString("email"));
				user.setCreditCardNumber(rs.getString("creditCardNumber"));
				user.setRewardPoint(rs.getInt("rewardPoint"));
				user.setMembership(rs.getString("membership"));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
		
		return user;
	}
	
	/**
	 * 判断用户是否存在...返回true说明不存在,false说明存在
	 * @param user
	 * @return
	 */
	public boolean isUserExist(User user) {
		boolean sign = true;
		String sql = "select * from user_info where name=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, user.getName());
			rs = pstmt.executeQuery();
			while(rs.next()) {
				sign = false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
		return sign;
	}

	
	/*
	 * 景点模块
	 * --------------------------------------------------------------------------------------------------------
	 */
	
	/**
	 * 列出所有景点名称
	 */
	public List listSightName() {
		String sql = "select * from sight_info";
		List sightList = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConn();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				Sight sight = new Sight();
				sight.setId(rs.getInt("id"));
				sight.setName(rs.getString("name"));
				sight.setCity(rs.getString("city"));
				sight.setCompany(rs.getString("company"));
				sight.setPrice(this.findPriceBySightName(sight.getName()));
				String sightInfo = rs.getString("info");
				if(sightInfo.length()>=12) {
					sightInfo = sightInfo.substring(0, 12) + "...";
				}
				sight.setInfo(sightInfo);
				sightList.add(sight);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(stmt);
			DB.closeConn(conn);
		}
		return sightList;
	}
	
	/**
	 * 根据条件列出景点
	 * @param name
	 * @param value
	 * @return
	 */
	public List querySight(String name, String value) {
		String sql = null;
		//根据传来的数据确定查询语句
		if(name.equals("city")) {
			sql = "select * from sight_info where city=?";
		} else if(name.equals("name")){
			sql = "select * from sight_info where name=?";
		} else if(name.equals("group_id")) {
			sql = "select * from sight_info where group_id=?";
		}
		
		List sightlList = new ArrayList();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			if(name.equals("group_id")) {
				int group_id = Integer.parseInt(value);
				pstmt.setInt(1, group_id);
			}
			rs = pstmt.executeQuery();
			while(rs.next()) {
				Sight sight = new Sight();
				sight.setCity(rs.getString("city"));
				sight.setName(rs.getString("name"));
				sight.setCompany(rs.getString("company"));
				sight.setInfo(rs.getString("info"));
				sightlList.add(sight);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
		
		return sightlList;
	}
	
	/**
	 * 根据Id查询景点
	 * @param id
	 * @return Sight
	 */
	public Sight findSightById(int id) {
		String sql = "select * from sight_info where id=?";
		Sight sight = new Sight();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				sight.setId(rs.getInt("id"));
				sight.setCity(rs.getString("city"));
				sight.setName(rs.getString("name"));
				sight.setCompany(rs.getString("company"));
				sight.setInfo(rs.getString("info"));
				sight.setPrice(this.findPriceBySightName(sight.getName()));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
		return sight;
	}
	
	/**
	 * 
	 * @param sight
	 * @return price
	 */
	private int findPriceBySightName(String sight)
	{
		int price=0;
		String sql = "select * from ticket_info where sight=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, sight);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				price=rs.getInt("price");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
		return price;
	}
	
	/**
	 * 根据城市名查询该城市的景点
	 * @param cityName
	 * @return
	 */
	public List listSightByCityName(String cityName) {
		String sql = "select * from sight_info where city=?";
		List sightList = new ArrayList();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, cityName);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				Sight sight = new Sight();
				sight.setId(rs.getInt("id"));
				sight.setName(rs.getString("name"));
				sight.setCity(rs.getString("city"));
				sight.setCompany(rs.getString("company"));
				sight.setPrice(this.findPriceBySightName(sight.getName()));
				String sightInfo = rs.getString("info");
				if(sightInfo.length()>=12) {
					sightInfo = sightInfo.substring(0, 12) + "...";
				}
				sight.setInfo(sightInfo);
				sightList.add(sight);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
		return sightList;
	}

	public List listSightBySightName(String sightName) {
		List sightList = new ArrayList();
		String sql = "select * from sight_info where name=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, sightName);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				Sight sight = new Sight();
				sight.setCity(rs.getString("city"));
				sight.setName(rs.getString("name"));
				sight.setCompany(rs.getString("company"));
				sight.setInfo(rs.getString("info"));
				sight.setId(rs.getInt("id"));
				sight.setPrice(this.findPriceBySightName(sight.getName()));
				sightList.add(sight);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
		return sightList;
	}
	
	public List listSightBySightCityName(String cityName,String sightName) {
		List sightList = new ArrayList();
		String sql = "select * from sight_info where name=?, city=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, sightName);
			pstmt.setString(2, cityName);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				Sight sight = new Sight();
				sight.setCity(rs.getString("city"));
				sight.setName(rs.getString("name"));
				sight.setInfo(rs.getString("info"));
				sight.setCompany(rs.getString("company"));
				sight.setId(rs.getInt("id"));
				sightList.add(sight);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
		return sightList;
	}
	
	
	public Sight findSightIdBySightName(String sightName) {
		String sql = "select * from sight_info where name=?";
		Sight sight = new Sight();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, sightName);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				sight.setId(rs.getInt("id"));
				sight.setName(rs.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
		return sight;
	}
	
	/**
	 * 更新景点信息
	 * @param sight
	 */
	public void updateSightInfo(Sight sight) {
		//update price of a ticket for that sight first
		updatePriceBySightName(sight.getPrice(),sight.getName());
		//then update other info
		String sql = "update sight_info set name=?,city=?,company=?,info=? where id=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, sight.getName());
			pstmt.setString(2, sight.getCity());
			pstmt.setString(3, sight.getCompany());
			pstmt.setString(4, sight.getInfo());
			pstmt.setInt(5, sight.getId());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
	}
	
	/**
	 * 
	 * @param sight
	 */
	private void updatePriceBySightName(int price, String sight)
	{
		String sql = "update ticket_info set price=? where sight=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, price);
			pstmt.setString(2, sight);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
	}
	
	/**
	 * 根据景点ID删除景点信息
	 * @param sightId
	 */
	public void deleteSightById(int sightId) {
		String sql = "delete from sight_info where id=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, sightId);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
	}
	
	/**
	 * 增加景点信息
	 * @param sight
	 */
	public void sightAdd(Sight sight) {
		Connection conn = null;
		conn = DB.getConn();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql = "select count(*) as id from sight_info";
			pstmt = conn.prepareStatement(sql);
			rs=pstmt.executeQuery(sql);
			while(rs.next())
			{
				sight.setId(rs.getInt("id"));
			}
			
			sql = "insert into sight_info(id, name, city, company, info) values(?, ?, ?, ?, ?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, sight.getId());
			pstmt.setString(2, sight.getName());
			pstmt.setString(3, sight.getCity());
			pstmt.setString(4, sight.getCompany());
			pstmt.setString(5, sight.getInfo());
			pstmt.executeUpdate();
			this.addTicketBySight(sight);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
	}
	
	/**
	 * 
	 * @param sight
	 */
	private void addTicketBySight(Sight sight)
	{
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		try {
			conn = DB.getConn();
			String sql = "select count(*) as id from ticket_info";
			pstmt = conn.prepareStatement(sql);
			rs=pstmt.executeQuery(sql);
			int id=0;
			while(rs.next())
			{
				id=rs.getInt("id");
			}
			
			sql = "insert into ticket_info(id, sight, city, price) values(?, ?, ?, ?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.setString(2, sight.getName());
			pstmt.setString(3, sight.getCity());
			pstmt.setInt(4, sight.getPrice());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
		
	}
	
	/**
	 * 只取出景点城市的名称,为首页所用.
	 * @return
	 */
	public List listCityNameForIndex() {
		String sql = "select * from sight_info";
		String city = null;
		List sightNameList = new ArrayList();
		List cityNameList=new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConn();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			 
			while(rs.next()) {
				Sight sight = new Sight();
				sight.setCity(rs.getString("city"));
				sight.setId(rs.getInt("id"));
				city=sight.getCity();
				if(!cityNameList.contains(city))
				{
					cityNameList.add(city);
					sightNameList.add(sight);
				}				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(stmt);
			DB.closeConn(conn);
		}
		return sightNameList;
	}
	
	
	/*
	 * 订票模块
	 * --------------------------------------------------------------------------------------------------------
	 */
	
	/**
	 * 根据sight name 查询 ticket
	 * @param sightName
	 * @return Ticket
	 */
	
	public Ticket findTicketBySightName(String sightName)
	{
		String sql = "select * from ticket_info where sight=?";
		Ticket ticket=new Ticket();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, sightName);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				ticket.setId(rs.getInt("id"));
				ticket.setSight(rs.getString("sight"));
				ticket.setCity(rs.getString("city"));
				ticket.setPrice(rs.getString("price"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
		return ticket;
	}
	
	/**
	 * 给用户订票
	 * @param bookInfo
	 */
	public void bookTicket(BookInfo bookInfo)
	{
		String sql = "insert into user_ticket(userId, ticketId, count, expirationDate) values(?, ?, ?, ?)";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, bookInfo.getUserId());
			pstmt.setInt(2, bookInfo.getTicketId());
			pstmt.setInt(3, bookInfo.getCount());
			pstmt.setString(4, bookInfo.getTicketExpirationDate());
			pstmt.executeUpdate();
			int totalPrice=this.getTotalPrice(bookInfo.getCount(), bookInfo.getTicketId());
			int radix=0;
			//choose to use rewardPoint or credit card to pay for it
			if(bookInfo.getUseReward())
			{
				radix=-10;
			}
			else
			{	
				String membership=this.findUserById(bookInfo.getUserId()).getMembership();
				if(membership.equals("silver"))
					radix=10;
				else if(membership.equals("gold"))
					radix=15;
				else radix=20;				
			}
			int rewardPoint=totalPrice*radix;
			this.addRewardPointByUserId(bookInfo.getUserId(), rewardPoint);
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
	}
	
	/**
	 * 获取当前用户的订票信息
	 * @param user
	 * @return UserTicket
	 */
	public List getUserTicketReservation(User user)
	{
		String sql = "select * from user_ticket where userId=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List tickets=new ArrayList();
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, user.getId());
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				UserTicket userTicket=new UserTicket();
				userTicket.setCount(rs.getInt("count"));
				userTicket.setExpirationDate(rs.getString("expirationDate"));
				String[] sightCity=this.getSightCityById(rs.getInt("ticketId"));
				userTicket.setSight(sightCity[0]);
				userTicket.setCity(sightCity[1]);
				tickets.add(userTicket);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
		return tickets;
	}
	
	
	private String[] getSightCityById(int ticketId)
	{
		String[] sightCity = new String[2];
		String sql = "select * from ticket_info where id=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List tickets=new ArrayList();
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, ticketId);
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				sightCity[0] = rs.getString("sight");
				sightCity[1] = rs.getString("city");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
		return sightCity;
	}
	/*
	 * 查看数据库模块
	 * --------------------------------------------------------------------------------------------------------
	 */
	
	/**
	 * 根据数据表名查询所有数据
	 * @param database name
	 * @return all data
	 */
	public String getDatabyDatabaseName(String database)
	{
		String sql = "select * from "+database;
		String result="<tr>";
		Connection conn = null;
		Statement stm = null;
		ResultSet rs = null;
		try {
			conn = DB.getConn();
			stm = conn.createStatement();
			rs = stm.executeQuery(sql);
			ResultSetMetaData meta=rs.getMetaData();
			int colC=meta.getColumnCount();
			for(int i=0;i<colC;i++)
			{
				result+="<td>"+meta.getColumnName(i+1)+"</td>";
			}
			result+="</tr>";
			while(rs.next()) {
				result+="<tr>";
				for(int i=0;i<colC;i++)
				{
					result+="<td>"+rs.getString(i+1)+"</td>";
				}
				result+="</tr>";
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(stm);
			DB.closeConn(conn);
		}
		return result;
	}
	
	/*
	 * 公用模块
	 * --------------------------------------------------------------------------------------------------------
	 */
	
	/**
	 * @param user_id
	 * @param rewardPoint
	 * 
	 */
	private boolean addRewardPointByUserId(int user_id, int rewardPoint)
	{
		boolean willUpdate=false;
		String sql = "update user_info set rewardPoint=? where user_id=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		int originReward=this.findUserById(user_id).getRewardPoint();
		rewardPoint+=originReward;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, rewardPoint);
			pstmt.setInt(2, user_id);
			pstmt.executeUpdate();
			int reward = this.findUserById(user_id).getRewardPoint();
			if(reward>=5000&&reward<10000)
				this.updateMembership("gold", user_id);
			else if(reward>=10000)
				this.updateMembership("platinum", user_id);
			else this.updateMembership("silver", user_id);
				
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
		return willUpdate;
	}
	
	private void updateMembership(String membership, int user_id)
	{
		String sql = "update user_info set membership=? where user_id=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, membership);
			pstmt.setInt(2, user_id);
			pstmt.executeUpdate();				
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
	}

	
	/**
	 * 
	 * @param count
	 * @param ticketId
	 * @return total price
	 */
	private int getTotalPrice(int count, int ticketId)
	{
		int total=0;
		int price=0;
		String sql = "select price from ticket_info where id=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConn();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, ticketId);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				price = rs.getInt("price");
			}
			if(price!=0)
				total=price*count;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.closeRS(rs);
			DB.closeStmt(pstmt);
			DB.closeConn(conn);
		}
		return total;
	}
	

}
